FROM multiarch/alpine:_DISTCROSS-edge
MAINTAINER Adrien le Maire <adrien@alemaire.be>
ARG RESTIC_VERSION
RUN apk add --update --no-cache ca-certificates fuse openssh-client curl && \
    curl -LOs https://github.com/restic/restic/releases/download/v${RESTIC_VERSION}/restic_${RESTIC_VERSION}_linux__GOCROSS.bz2 && \
    bunzip2 restic_${RESTIC_VERSION}_linux__GOCROSS.bz2 && \
    mv restic_${RESTIC_VERSION}_linux__GOCROSS /usr/bin/restic && chmod +x /usr/bin/restic

ENTRYPOINT ["/usr/bin/restic"]
